[![Get it on Google Play](https://play.google.com/intl/en_gb/badges/images/generic/en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.damsky.danny.schoolassist)

### What is this repository for? ###

School Assist is a FOSS application for Android designed to help the student keep track of their weekly schedule. 

The application supports Android Lollipop and above.

School Assist is written in Kotlin (Aside from DB folder), uses GreenDAO 3 ORM for the Database and
Glide for picture loading.

Other libraries used include:

*  [AppIntro](https://github.com/apl-devs/AppIntro)

This repository is for those who want an easy solution to help them stay on track of their life at school.

### Who do I talk to? ###

My name: Danny Damsky 

E-mail: dannydamsky99@gmail.com

LinkedIn: https://www.linkedin.com/in/danny-damsky/

### LICENSE ###

   Copyright 2017 Danny Damsky

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.