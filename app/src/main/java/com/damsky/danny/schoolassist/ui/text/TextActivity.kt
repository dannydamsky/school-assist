package com.damsky.danny.schoolassist.ui.text

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.utils.Constants
import kotlinx.android.synthetic.main.activity_text.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

class TextActivity : AppCompatActivity() {
    private lateinit var file: File
    private lateinit var display: Display
    private lateinit var originalContent: String

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme((application as App).preferencesHelper.getThemeNoActionBar())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text)
        setSupportActionBar(toolbar)

        display = Display(this, R.drawable.ic_about_launcher)

        file = File(intent.getStringExtra(Constants.EXTRA_DIRECTORY))

        originalContent = intent.getStringExtra(Constants.EXTRA_CONTENT)
        editText.setText(originalContent)

        supportActionBar?.subtitle = file.nameWithoutExtension
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun finish() {
        val newContent = editText.text.toString()
        if (newContent == originalContent) {
            setResult(RESULT_CANCELED)
            super.finish()
        } else {
            display.showBasicDialog(R.string.save_changes, R.string.save_changes_message, {
                saveFile(newContent)
                intent.putExtra(Constants.EXTRA_CONTENT, newContent)
                setResult(RESULT_OK, intent)
                super.finish()
            }, {
                super.finish()
            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onPause() {
        intent.putExtra(Constants.EXTRA_CONTENT, editText.text.toString())
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_text, menu)
        return true
    }

    fun onSharePressed(item: MenuItem) {
        val stringBuilder = StringBuilder()

        stringBuilder.append(file.nameWithoutExtension)
                .append("\n")
                .append("\n")
                .append(editText.text)

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, stringBuilder.toString())

        try {
            startActivity(Intent.createChooser(intent, resources.getString(R.string.send_via)))
        } catch (ex: android.content.ActivityNotFoundException) {
            display.showSnackShort(R.string.error_share)
        }

    }

    private fun saveFile(newContent: String) {
        try {
            BufferedWriter(FileWriter(file)).use {
                it.write(newContent)
            }
        } catch (e: Exception) {
            display.showToastShort(R.string.error_save)
        }
    }
}
