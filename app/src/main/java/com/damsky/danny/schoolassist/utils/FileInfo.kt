package com.damsky.danny.schoolassist.utils

import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class FileInfo {
    val file: File
    val isText: Boolean
    private var content: StringBuilder

    constructor(fileDir: String) {
        this.file = File(fileDir)
        this.isText = this.file.extension == "txt"
        this.content = StringBuilder()

        init()
    }

    constructor(fileDir: String, content: String) {
        this.file = File(fileDir)
        this.isText = true
        this.content = StringBuilder(content)
    }

    constructor(file: File) {
        this.file = file
        this.isText = file.extension == "txt"
        this.content = StringBuilder()

        init()
    }

    fun renameFileTo(fileName: String) {
        val nameWithoutSpaces = fileName.replace(" ", "")
        val newFile = file.parent + File.separator + nameWithoutSpaces + "." + file.extension
        file.renameTo(File(newFile))
    }

    fun setContent(newContent: String) {
        this.content = StringBuilder(newContent)
    }

    private fun init() {
        if (isText) {
            try {
                BufferedReader(FileReader(file)).use {
                    var text = it.readLine()
                    while (text != null) {
                        content.append(text).append("\n")
                        text = it.readLine()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun toString() = content.toString()


    override fun equals(other: Any?): Boolean {
        if (other is FileInfo) {
            if (this.isText && other.isText)
                return this.content.toString() == other.content.toString()
            else if (!this.isText && !other.isText)
                return this.file.absolutePath == other.file.absolutePath
        }

        return false
    }

    override fun hashCode(): Int {
        var result = file.hashCode()
        result = 31 * result + isText.hashCode()
        result = 31 * result + content.hashCode()
        return result
    }
}
