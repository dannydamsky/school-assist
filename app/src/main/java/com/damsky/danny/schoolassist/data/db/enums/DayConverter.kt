package com.damsky.danny.schoolassist.data.db.enums

import org.greenrobot.greendao.DaoException
import org.greenrobot.greendao.converter.PropertyConverter

class DayConverter : PropertyConverter<Day, Int> {

    override fun convertToEntityProperty(databaseValue: Int?): Day? {
        if (databaseValue == null)
            return null

        Day.values()
                .filter { it.day == databaseValue }
                .forEach { return it }

        throw DaoException("Error converting following Integer to Day: $databaseValue")

    }

    override fun convertToDatabaseValue(entityProperty: Day): Int? = entityProperty.day
}
