package com.damsky.danny.schoolassist.data.db.enums

import java.util.*

enum class Day (val day: Int) {
    SUNDAY (Calendar.SUNDAY),
    MONDAY (Calendar.MONDAY),
    TUESDAY (Calendar.TUESDAY),
    WEDNESDAY (Calendar.WEDNESDAY),
    THURSDAY (Calendar.THURSDAY),
    FRIDAY (Calendar.FRIDAY),
    SATURDAY (Calendar.SATURDAY);
}
