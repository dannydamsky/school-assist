package com.damsky.danny.schoolassist.utils

class Constants {
    companion object {
        /**
         * Default values.
         */
        const val DAYS_IN_WEEK = 7
        const val MAX_LESSONS = 21
        const val DEFAULT_NOTIFICATION_DELTA = "30"
        const val DEFAULT_SCHOOL_URL = "http://www.haderahigh.org.il/"
        const val DEFAULT_SMART_SCHOOL_URL = "https://www.webtop.co.il/mobile/?"

        /**
         * URLs.
         */
        const val URL_SOURCE_CODE = "https://bitbucket.org/dannydamsky/school-assist/src"
        const val URL_EMAIL = "mailto:dannydamskypublic@gmail.com"
        const val URL_STORE = "market://details?id=com.damsky.danny.schoolassist"
        const val URL_STORE_BACKUP = "https://play.google.com/store/apps/details?id=com.damsky.danny.schoolassist"
        const val URL_DONATE = "market://details?id=com.damsky.danny.schoolassistpro"
        const val URL_DONATE_BACKUP = "https://play.google.com/store/apps/details?id=com.damsky.danny.schoolassistpro"

        /**
         * The name of the DB file used for storing information about lessons.
         */
        const val DB_NAME = "lessons-db"

        /**
         * Preferences keys.
         */
        const val PREFERENCES_NAME = "my_preferences"
        const val PREFERENCE_FIRST_RUN = "first_run_preference"
        const val PREFERENCE_APP_THEME = "app_theme_preferences"
        const val PREFERENCE_SCHOOL = "change_web_school"
        const val PREFERENCE_SMART_SCHOOL = "change_web_smart"
        const val PREFERENCE_NOTIFICATION_DELTA = "change_notification_delta"

        /**
         * Request codes.
         */
        const val REQUEST_CODE_NOTE = 1
        const val REQUEST_CODE_CAMERA = 2
        const val REQUEST_CODE_GALLERY = 3
        const val REQUEST_CODE_START_INTRO = 101
        const val REQUEST_CODE_START_ACTIONS = 102
        const val REQUEST_CODE_START_TIMES = 103
        const val REQUEST_CODE_NOTIFICATIONS = 325
        const val REQUEST_CODE_PERMISSIONS = 458

        /**
         * Intent extras.
         */
        const val EXTRA_DIRECTORY = "directory"
        const val EXTRA_TITLE = "title"
        const val EXTRA_SUB_TITLE = "subtitle"
        const val EXTRA_MESSAGE = "message"
        const val EXTRA_URL = "url"
        const val EXTRA_URL_CURRENT = "current_url"
        const val EXTRA_CONTENT = "content"
        const val EXTRA_LESSON_START_HOURS = "startHours"
        const val EXTRA_LESSON_START_MINUTES = "startMinutes"
        const val EXTRA_LESSON_END_HOURS = "endHours"
        const val EXTRA_LESSON_END_MINUTES = "endMinutes"
        const val EXTRA_NOTE_NAME = "note_name"

        /**
         * Notification details.
         */
        const val NOTIFICATION_ID = "lesson_notification_id"
        const val NOTIFICATION_DESCRIPTION = "Lesson Notifications"

        /**
         * Indexes.
         */
        const val ACTIONS_SET_NOTIFICATIONS = 0
        const val ACTIONS_DISABLE_NOTIFICATIONS = 1
        const val ACTIONS_ENABLE_NOTIFICATIONS = 2
        const val ACTIONS_DELETE_NOTIFICATIONS = 3
    }
}
