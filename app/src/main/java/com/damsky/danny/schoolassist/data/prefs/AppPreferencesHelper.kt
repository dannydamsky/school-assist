package com.damsky.danny.schoolassist.data.prefs

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatDelegate
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.utils.Constants

class AppPreferencesHelper(private val context: Context) {

    private fun updateBoolean(prefName: String, boolean: Boolean) {
        val editor = getSharedPreferences().edit()
        editor.putBoolean(prefName, boolean)
        editor.apply()
    }

    private fun getSharedPreferences(): SharedPreferences =
            context.getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE)

    private fun getDefaultSharedPreferences(): SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)

    fun isFirstRun(boolean: Boolean) = updateBoolean(Constants.PREFERENCE_FIRST_RUN, boolean)

    fun isFirstRun(): Boolean = getSharedPreferences().getBoolean(Constants.PREFERENCE_FIRST_RUN, true)

    fun getTheme(): Int {
        val appThemesValues = context.resources.getStringArray(R.array.app_themes_values)
        if (getDefaultSharedPreferences().getString(Constants.PREFERENCE_APP_THEME, appThemesValues[0]) == appThemesValues[2])
            return R.style.AppTheme_Black

        return R.style.AppTheme
    }

    fun getThemeNoActionBar(): Int {
        val appThemesValues = context.resources.getStringArray(R.array.app_themes_values)
        if (getDefaultSharedPreferences().getString(Constants.PREFERENCE_APP_THEME, appThemesValues[0]) == appThemesValues[2])
            return R.style.AppTheme_Black_NoActionBar

        return R.style.AppTheme_NoActionBar
    }

    fun getNightMode(): Int {
        val appThemesValues = context.resources.getStringArray(R.array.app_themes_values)
        if (getDefaultSharedPreferences().getString(Constants.PREFERENCE_APP_THEME, appThemesValues[0]) == appThemesValues[0])
            return AppCompatDelegate.MODE_NIGHT_NO

        return AppCompatDelegate.MODE_NIGHT_YES
    }

    fun getThemeAndDayNightMode(): Pair<Int, Int> {
        val appThemesValues = context.resources.getStringArray(R.array.app_themes_values)
        when (getDefaultSharedPreferences().getString(Constants.PREFERENCE_APP_THEME, appThemesValues[0])) {
            appThemesValues[0] ->
                return Pair(R.style.AppTheme, AppCompatDelegate.MODE_NIGHT_NO)

            appThemesValues[1] ->
                return Pair(R.style.AppTheme, AppCompatDelegate.MODE_NIGHT_YES)

            appThemesValues[2] ->
                return Pair(R.style.AppTheme_Black, AppCompatDelegate.MODE_NIGHT_YES)
        }

        return Pair(-1, -1)
    }

    fun getThemeAndDayNightModeNoActionBar(): Pair<Int, Int> {
        val appThemesValues = context.resources.getStringArray(R.array.app_themes_values)
        when (getDefaultSharedPreferences().getString(Constants.PREFERENCE_APP_THEME, appThemesValues[0])) {
            appThemesValues[0] ->
                return Pair(R.style.AppTheme_NoActionBar, AppCompatDelegate.MODE_NIGHT_NO)

            appThemesValues[1] ->
                return Pair(R.style.AppTheme_NoActionBar, AppCompatDelegate.MODE_NIGHT_YES)

            appThemesValues[2] ->
                return Pair(R.style.AppTheme_Black_NoActionBar, AppCompatDelegate.MODE_NIGHT_YES)
        }

        return Pair(-1, -1)
    }

    fun getSchoolUrl(): String = getDefaultSharedPreferences().getString(Constants.PREFERENCE_SCHOOL,
            Constants.DEFAULT_SCHOOL_URL)

    fun getSmartUrl(): String =
            getDefaultSharedPreferences().getString(Constants.PREFERENCE_SMART_SCHOOL,
                    Constants.DEFAULT_SMART_SCHOOL_URL)

    fun getNotificationDelta(): Long =
            getDefaultSharedPreferences().getString(Constants.PREFERENCE_NOTIFICATION_DELTA,
                    Constants.DEFAULT_NOTIFICATION_DELTA).toLong()
}
