package com.damsky.danny.schoolassist.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.damsky.danny.schoolassist.data.db.model.Lesson
import com.damsky.danny.schoolassist.receivers.NotificationsReceiver
import java.util.*
import java.util.concurrent.TimeUnit

class NotificationTimer {
    companion object {
        const val DAY_IN_MILLIS = 86_400_000
        const val WEEK_IN_MILLIS = 604_800_000

        fun getDisplay(hours: Int, minutes: Int): String {
            val timeString = StringBuilder()
            if (hours < 10)
                timeString.append(0)
            timeString.append(hours).append(':')

            if (minutes < 10)
                timeString.append(0)
            timeString.append(minutes)

            return timeString.toString()

        }

        fun getDisplay(calendar: Calendar): String {
            val hours = calendar.get(Calendar.HOUR_OF_DAY)
            val minutes = calendar.get(Calendar.MINUTE)
            return getDisplay(hours, minutes)
        }

        fun getDisplayRange(fromHours: Int, fromMinutes: Int, toHours: Int, toMinutes: Int) =
                "${getDisplay(fromHours, fromMinutes)}-${getDisplay(toHours, toMinutes)}"

        fun getDisplayRange(fromCalendar: Calendar, toCalendar: Calendar) =
                "${getDisplay(fromCalendar)}-${getDisplay(toCalendar)}"

        fun setNotification(context: Context, lesson: Lesson, notificationDelta: Long) {
            if (lesson.notify) {
                val pair = init(lesson, notificationDelta)
                val start = pair.first
                val end = pair.second

                val broadcastIntent = Intent(context, NotificationsReceiver::class.java)
                broadcastIntent.putExtra(Constants.EXTRA_TITLE, "${lesson.lessonNum}: ${lesson.lessonName}")
                broadcastIntent.putExtra(Constants.EXTRA_MESSAGE, getDisplayRange(start, end))

                notify(context, broadcastIntent, lesson.id.toInt(), start, notificationDelta)
            } else
                unNotify(context, lesson.id.toInt())
        }

        private fun init(lesson: Lesson, notificationDelta: Long): Pair<Calendar, Calendar> {
            val start = Calendar.getInstance()
            start.set(Calendar.DAY_OF_WEEK, lesson.lessonDay.day)
            start.set(Calendar.HOUR_OF_DAY, lesson.startHour)
            start.set(Calendar.MINUTE, lesson.startMinute)
            start.set(Calendar.SECOND, 0)

            val end = Calendar.getInstance()
            end.set(Calendar.DAY_OF_WEEK, lesson.lessonDay.day)
            end.set(Calendar.HOUR_OF_DAY, lesson.endHour)
            end.set(Calendar.MINUTE, lesson.endMinute)
            end.set(Calendar.SECOND, 0)

            fixCalendarTimes(start, end, notificationDelta)

            return Pair(start, end)
        }

        private fun fixCalendarTimes(start: Calendar, end: Calendar, notificationDelta: Long) {
            val calendar = Calendar.getInstance()

            if (getWhenToNotifyInMillis(start, notificationDelta) <= calendar.timeInMillis) {
                start.timeInMillis += WEEK_IN_MILLIS
                end.timeInMillis += WEEK_IN_MILLIS
            }

            if (end.timeInMillis <= start.timeInMillis)
                end.timeInMillis += DAY_IN_MILLIS
        }

        private fun getWhenToNotifyInMillis(start: Calendar, notificationDelta: Long): Long =
                start.timeInMillis - TimeUnit.MINUTES.toMillis(notificationDelta)

        private fun notify(context: Context, intent: Intent, lessonId: Int, start: Calendar, notificationDelta: Long) {
            val pendingIntent = PendingIntent.getBroadcast(context, lessonId, intent, 0)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, getWhenToNotifyInMillis(start, notificationDelta),
                    AlarmManager.INTERVAL_DAY * Constants.DAYS_IN_WEEK, pendingIntent)
        }

        private fun unNotify(context: Context, lessonId: Int) {
            val pendingIntent = PendingIntent.getBroadcast(context, lessonId,
                    Intent(context, NotificationsReceiver::class.java), 0)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager.cancel(pendingIntent)
        }
    }
}
