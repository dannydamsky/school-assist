package com.damsky.danny.schoolassist.data.db

import android.content.Context
import com.damsky.danny.schoolassist.data.db.enums.Day
import com.damsky.danny.schoolassist.data.db.model.DaoMaster
import com.damsky.danny.schoolassist.data.db.model.DaoSession
import com.damsky.danny.schoolassist.data.db.model.Lesson
import com.damsky.danny.schoolassist.data.db.model.LessonDao
import com.damsky.danny.schoolassist.utils.Constants
import kotlin.Array

class AppDbHelper(private val context: Context) {
    private val lessons: Array<ArrayList<Lesson>>

    init {
        val queryList = getDaoSession().lessonDao.queryBuilder()
                .orderAsc(LessonDao.Properties.LessonDay)
                .orderAsc(LessonDao.Properties.LessonNum)
                .build()
                .list()

        var j = 0
        lessons = Array(Constants.DAYS_IN_WEEK, { i ->
            val arrayList = ArrayList<Lesson>(0)

            while (j < queryList.size && i == queryList[j].lessonDay.day - 1) {
                arrayList.add(queryList[j])
                j++
            }

            return@Array arrayList
        })
    }

    fun getDaoSession(): DaoSession =
            DaoMaster(DaoMaster.DevOpenHelper(context, Constants.DB_NAME).writableDb).newSession()

    fun insertLesson(lessonName: String, lessonNum: Int, lessonDay: Day, daoSession: DaoSession = getDaoSession()) {
        val lesson = Lesson(null, lessonName, lessonNum, false, lessonDay, 11, 12, 0, 0)
        daoSession.insert(lesson)
        val day = lessons[lessonDay.day - 1]
        day.add(lesson)
        day.sortWith(kotlin.Comparator { o1, o2 ->
            return@Comparator o1.lessonNum.compareTo(o2.lessonNum)
        })
    }

    fun updateLesson(lessonToUpdate: Lesson, newName: String, daoSession: DaoSession = getDaoSession()) {
        lessonToUpdate.lessonName = newName
        updateLesson(lessonToUpdate, daoSession)
    }

    fun updateLesson(lessonToUpdate: Lesson, daoSession: DaoSession = getDaoSession()) =
            daoSession.lessonDao.update(lessonToUpdate)

    fun updateLesson(lessonToUpdate: Lesson, startHour: Int, startMinute: Int, endHour: Int, endMinute: Int, daoSession: DaoSession = getDaoSession()) {
        lessonToUpdate.startHour = startHour
        lessonToUpdate.startMinute = startMinute
        lessonToUpdate.endHour = endHour
        lessonToUpdate.endMinute = endMinute
        lessonToUpdate.notify = true

        updateLesson(lessonToUpdate, daoSession)
    }

    fun deleteLesson(lessonToDelete: Lesson, daoSession: DaoSession = getDaoSession()) {
        daoSession.delete(lessonToDelete)
        lessons[lessonToDelete.lessonDay.day - 1].remove(lessonToDelete)
    }

    fun deleteAllLessons(daoSession: DaoSession = getDaoSession()) {
        daoSession.lessonDao.deleteAll()

        for (i in lessons)
            i.clear()
    }

    fun getLessons(dayNumber: Int): ArrayList<Lesson> = lessons[dayNumber]

    fun getLessons(): Array<ArrayList<Lesson>> = lessons

    fun getLessonNames(suggestionsArrayList: ArrayList<String>): ArrayList<String> {
        val set = HashSet<String>(suggestionsArrayList)
        for (day in lessons)
            for (lesson in day)
                set.add(lesson.lessonName)

        val arrayList = ArrayList<String>(set.size)
        arrayList.addAll(set)
        return arrayList
    }

    fun getLessonNumbers(): ArrayList<Int> {
        val set = HashSet<Int>()
        for (day in lessons)
            for (lesson in day)
                set.add(lesson.lessonNum)

        val arrayList = ArrayList<Int>(set.size)
        arrayList.addAll(set)
        arrayList.sort()
        return arrayList
    }

    fun isEmpty(): Boolean {
        for (day in lessons)
            if (day.isNotEmpty())
                return false

        return true
    }
}
