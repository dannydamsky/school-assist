package com.damsky.danny.schoolassist.utils

data class LessonNumber(val lessonNumber: Int, var startHour: Int = 11, var startMinute: Int = 0, var endHour: Int = 12, var endMinute: Int = 0) {

    fun getStartTime(): String {
        return NotificationTimer.getDisplay(startHour, startMinute)
    }

    fun getEndTime(): String {
        return NotificationTimer.getDisplay(endHour, endMinute)
    }

    fun setStartTime(startHour: Int, startMinute: Int) {
        this.startHour = startHour
        this.startMinute = startMinute
    }

    fun setEndTime(endHour: Int, endMinute: Int) {
        this.endHour = endHour
        this.endMinute = endMinute
    }

    override fun toString(): String {
        return "$lessonNumber ${getStartTime()} ${getEndTime()}"
    }
}
