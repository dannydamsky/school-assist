package com.damsky.danny.schoolassist.ui.actions.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.damsky.danny.schoolassist.R

class ActionsListViewAdapter(context: Context, private val actionList: List<String>) :
        ArrayAdapter<String>(context, R.layout.activity_actions_listview, actionList) {

    private class ViewHolder(view: View) {
        internal val actionNumber: TextView = view.findViewById(R.id.actionNumber)
        internal val actionText: TextView = view.findViewById(R.id.actionText)
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.activity_actions_listview, parent, false)
            viewHolder = ViewHolder(view)

            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        viewHolder.actionNumber.text = "${position + 1}"
        viewHolder.actionText.text = actionList[position]

        return view
    }
}
