package com.damsky.danny.schoolassist.receivers

import android.annotation.TargetApi
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.splash.SplashActivity
import com.damsky.danny.schoolassist.utils.Constants

class NotificationsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        createChannel(context)

        val builder = NotificationCompat.Builder(context, Constants.NOTIFICATION_ID)
        builder.setContentTitle(intent.getStringExtra(Constants.EXTRA_TITLE))
        builder.setContentText(intent.getStringExtra(Constants.EXTRA_MESSAGE))
        builder.setSmallIcon(R.mipmap.ic_foreground)

        val notificationIntent = Intent(context, SplashActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0)

        builder.setContentIntent(pendingIntent)
        builder.setAutoCancel(true)

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(Constants.REQUEST_CODE_NOTIFICATIONS, builder.build())
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE) as NotificationManager

            val mChannel = android.app.NotificationChannel(Constants.NOTIFICATION_ID,
                    Constants.NOTIFICATION_DESCRIPTION,
                    android.app.NotificationManager.IMPORTANCE_HIGH)
            mChannel.description = Constants.NOTIFICATION_DESCRIPTION
            mChannel.setShowBadge(false)
            mChannel.lockscreenVisibility = android.app.Notification.VISIBILITY_PUBLIC

            mNotificationManager.createNotificationChannel(mChannel)
        }
    }
}
