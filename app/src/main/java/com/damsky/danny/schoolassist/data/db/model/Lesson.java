package com.damsky.danny.schoolassist.data.db.model;

import com.damsky.danny.schoolassist.data.db.enums.Day;
import com.damsky.danny.schoolassist.data.db.enums.DayConverter;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(active = true, nameInDb = "LESSONS")
public class Lesson {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String lessonName;

    @NotNull
    private int lessonNum;

    @NotNull
    private boolean notify;

    @NotNull
    @Convert(converter = DayConverter.class, columnType = Integer.class)
    private Day lessonDay;

    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 610143130)
    private transient LessonDao myDao;
    @Generated(hash = 1353645318)
    public Lesson(Long id, @NotNull String lessonName, int lessonNum,
            boolean notify, @NotNull Day lessonDay, int startHour, int endHour,
            int startMinute, int endMinute) {
        this.id = id;
        this.lessonName = lessonName;
        this.lessonNum = lessonNum;
        this.notify = notify;
        this.lessonDay = lessonDay;
        this.startHour = startHour;
        this.endHour = endHour;
        this.startMinute = startMinute;
        this.endMinute = endMinute;
    }
    @Generated(hash = 1669664117)
    public Lesson() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLessonName() {
        return this.lessonName;
    }
    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }
    public int getLessonNum() {
        return this.lessonNum;
    }
    public void setLessonNum(int lessonNum) {
        this.lessonNum = lessonNum;
    }
    public boolean getNotify() {
        return this.notify;
    }
    public void setNotify(boolean notify) {
        this.notify = notify;
    }
    public Day getLessonDay() {
        return this.lessonDay;
    }
    public void setLessonDay(Day lessonDay) {
        this.lessonDay = lessonDay;
    }
    public int getStartHour() {
        return this.startHour;
    }
    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }
    public int getEndHour() {
        return this.endHour;
    }
    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }
    public int getStartMinute() {
        return this.startMinute;
    }
    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }
    public int getEndMinute() {
        return this.endMinute;
    }
    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2078826279)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getLessonDao() : null;
    }
}
