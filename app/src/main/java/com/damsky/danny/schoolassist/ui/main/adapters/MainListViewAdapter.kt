package com.damsky.danny.schoolassist.ui.main.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.data.db.model.Lesson
import com.damsky.danny.schoolassist.utils.NotificationTimer

class MainListViewAdapter(context: Context, lessons: ArrayList<Lesson>) :
        ArrayAdapter<Lesson>(context, R.layout.fragment_main_listview, lessons) {

    private class ViewHolder(view: View) {
        internal val lessonNumber: TextView = view.findViewById(R.id.lessonNumber)
        internal val mainText: TextView = view.findViewById(R.id.mainText)
        internal val contentDuration: TextView = view.findViewById(R.id.contentDuration)
        internal val contentNotification: ImageView = view.findViewById(R.id.contentNotification)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder: ViewHolder
        val view: View
        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.fragment_main_listview, parent, false)
            viewHolder = ViewHolder(view)

            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        setupContent(viewHolder, getItem(position))

        return view
    }

    private fun setupContent(viewHolder: ViewHolder, lesson: Lesson) {
        viewHolder.lessonNumber.text = "${lesson.lessonNum}"
        viewHolder.mainText.text = lesson.lessonName
        viewHolder.contentDuration.text = NotificationTimer.getDisplayRange(lesson.startHour,
                lesson.startMinute, lesson.endHour, lesson.endMinute)

        if (lesson.notify) {
            viewHolder.contentNotification.visibility = View.VISIBLE
            viewHolder.contentDuration.visibility = View.VISIBLE
        }
    }
}
