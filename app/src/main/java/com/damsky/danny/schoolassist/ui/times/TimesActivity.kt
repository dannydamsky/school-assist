package com.damsky.danny.schoolassist.ui.times

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Surface
import android.view.WindowManager
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.times.adapters.TimesAdapter
import com.damsky.danny.schoolassist.ui.times.listeners.TimesClickListener
import com.damsky.danny.schoolassist.utils.Constants
import com.damsky.danny.schoolassist.utils.LessonNumber
import kotlinx.android.synthetic.main.activity_times.*

class TimesActivity : AppCompatActivity(), TimesClickListener {

    private lateinit var times: ArrayList<LessonNumber>
    private lateinit var appReference: App
    private lateinit var display: Display

    private fun setAdapter() {
        val adapter = TimesAdapter(this, times)
        myTimes.adapter = adapter
    }

    private fun isLandscape(): Boolean {
        val rotationDegrees = (getSystemService(WINDOW_SERVICE) as WindowManager)
                .defaultDisplay
                .rotation

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        appReference = application as App
        setTheme(appReference.preferencesHelper.getThemeNoActionBar())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_times)
        setSupportActionBar(toolbar)

        display = Display(this, R.drawable.ic_about_launcher)

        myTimes.layoutManager = if (isLandscape())
            GridLayoutManager(this, 2)
        else
            LinearLayoutManager(this)

        myTimes.itemAnimator = DefaultItemAnimator()

        val numberList = appReference.appDbHelper.getLessonNumbers()
        times = ArrayList(numberList.size)
        for (lessonNumber in numberList)
            times.add(LessonNumber(lessonNumber))

        val startHours = intent.getIntArrayExtra(Constants.EXTRA_LESSON_START_HOURS)
        val startMinutes = intent.getIntArrayExtra(Constants.EXTRA_LESSON_START_MINUTES)
        val endHours = intent.getIntArrayExtra(Constants.EXTRA_LESSON_END_HOURS)
        val endMinutes = intent.getIntArrayExtra(Constants.EXTRA_LESSON_END_MINUTES)

        if (startHours != null && startMinutes != null && endHours != null && endMinutes != null) {
            for (i in 0 until times.size) {
                val index = times[i]
                index.startHour = startHours[i]
                index.startMinute = startMinutes[i]
                index.endHour = endHours[i]
                index.endMinute = endMinutes[i]
            }
        }

        setAdapter()

        discardButton.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }

        applyButton.setOnClickListener {
            val daoSession = appReference.appDbHelper.getDaoSession()

            for (day in appReference.appDbHelper.getLessons())
                for (lesson in day)
                    for (time in times)
                        if (time.lessonNumber == lesson.lessonNum) {
                            appReference.appDbHelper.updateLesson(lesson,
                                    time.startHour,
                                    time.startMinute,
                                    time.endHour,
                                    time.endMinute, daoSession)
                        }

            appReference.setAllNotifications()
            setResult(RESULT_OK)
            finish()
        }

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onPause() {
        val startHours = IntArray(times.size)
        val startMinutes = IntArray(times.size)
        val endHours = IntArray(times.size)
        val endMinutes = IntArray(times.size)

        for (i in 0 until times.size) {
            val index = times[i]
            startHours[i] = index.startHour
            startMinutes[i] = index.startMinute
            endHours[i] = index.endHour
            endMinutes[i] = index.endMinute
        }

        intent.putExtra(Constants.EXTRA_LESSON_START_HOURS, startHours)
        intent.putExtra(Constants.EXTRA_LESSON_START_MINUTES, startMinutes)
        intent.putExtra(Constants.EXTRA_LESSON_END_HOURS, endHours)
        intent.putExtra(Constants.EXTRA_LESSON_END_MINUTES, endMinutes)
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {
        setResult(RESULT_CANCELED)
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onStartTimeClicked(position: Int) {
        display.showTimeDialog(R.string.lesson_notify_start, times[position].startHour,
                times[position].startMinute, { hourOfDay: Int, minute: Int ->
            times[position].setStartTime(hourOfDay, minute)

            myTimes.adapter.notifyDataSetChanged()
        })
    }

    override fun onEndTimeClicked(position: Int) {
        display.showTimeDialog(R.string.lesson_notify_end, times[position].endHour,
                times[position].endMinute, { hourOfDay: Int, minute: Int ->
            times[position].setEndTime(hourOfDay, minute)

            myTimes.adapter.notifyDataSetChanged()
        })
    }
}
