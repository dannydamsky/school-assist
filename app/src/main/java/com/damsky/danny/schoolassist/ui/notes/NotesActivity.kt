package com.damsky.danny.schoolassist.ui.notes

import android.content.Intent
import android.graphics.drawable.AnimatedVectorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Surface
import android.view.WindowManager
import android.widget.EditText
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.notes.adapters.CardAdapter
import com.damsky.danny.schoolassist.ui.notes.listeners.CardClickListener
import com.damsky.danny.schoolassist.ui.text.TextActivity
import com.damsky.danny.schoolassist.utils.Constants
import com.damsky.danny.schoolassist.utils.FileInfo
import kotlinx.android.synthetic.main.activity_notes.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class NotesActivity : AppCompatActivity(), CardClickListener {
    private lateinit var directory: String
    private lateinit var display: Display
    private lateinit var files: ArrayList<FileInfo>
    private var showFabs = false

    private fun isLandscape(): Boolean {
        val rotationDegrees = (getSystemService(WINDOW_SERVICE) as WindowManager)
                .defaultDisplay
                .rotation

        return rotationDegrees == Surface.ROTATION_90 || rotationDegrees == Surface.ROTATION_270
    }

    private fun populateArrayListOfFiles() {
        val filesInFolder = File(directory).listFiles()
        files = ArrayList(filesInFolder.size)
        filesInFolder.iterator().forEach { file ->
            files.add(FileInfo(file))
        }
    }

    private fun setAdapter() {
        val adapter = CardAdapter(this, files)
        myNotes.adapter = adapter
    }

    private fun enableFabs() {
        mainFab.setImageResource(R.drawable.avd_plus_to_cross)
        (mainFab.drawable as AnimatedVectorDrawable).start()
        fab1.show()
        fab2.show()
        fab3.show()
        showFabs = true
    }

    private fun disableFabs() {
        mainFab.setImageResource(R.drawable.avd_cross_to_plus)
        (mainFab.drawable as AnimatedVectorDrawable).start()
        fab1.hide()
        fab2.hide()
        fab3.hide()
        showFabs = false
    }

    private fun toggleFabs() {
        if (showFabs)
            disableFabs()
        else
            enableFabs()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme((application as App).preferencesHelper.getThemeNoActionBar())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        setSupportActionBar(toolbar)

        display = Display(this, R.drawable.ic_about_launcher, snackbarNotes)

        myNotes.hasFixedSize()
        if (isLandscape())
            myNotes.layoutManager = GridLayoutManager(this, 2)
        else
            myNotes.layoutManager = LinearLayoutManager(this)
        myNotes.itemAnimator = DefaultItemAnimator()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        try {
            supportActionBar?.subtitle = intent.getStringExtra(Constants.EXTRA_SUB_TITLE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        directory = intent.getStringExtra(Constants.EXTRA_DIRECTORY)

        mainFab.setOnClickListener { toggleFabs() }
        fab1.setOnClickListener {
            toggleFabs()
            addGalleryNote()
        }

        fab2.setOnClickListener {
            toggleFabs()
            addPhotoNote()
        }

        fab3.setOnClickListener {
            toggleFabs()
            addTextNote()
        }

        populateArrayListOfFiles()
        setAdapter()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.REQUEST_CODE_NOTE -> {
                if (resultCode == RESULT_OK) {
                    val filePath = data!!.getStringExtra(Constants.EXTRA_DIRECTORY)
                    val newContent = data.getStringExtra(Constants.EXTRA_CONTENT)

                    var boolean = false
                    for (file in files) {
                        if (file.file.absolutePath == filePath) {
                            file.setContent(newContent)
                            boolean = true
                            break
                        }
                    }

                    if (!boolean)
                        files.add(FileInfo(filePath, newContent))

                    myNotes.adapter.notifyDataSetChanged()
                }
            }

            Constants.REQUEST_CODE_CAMERA -> {
                val photoFile = File(intent.getStringExtra(Constants.EXTRA_DIRECTORY))

                if (resultCode == RESULT_OK) {
                    val noteName = data!!.getStringExtra(Constants.EXTRA_NOTE_NAME)
                    renamePhotoFile(noteName, photoFile)
                } else
                    photoFile.delete()
            }

            Constants.REQUEST_CODE_GALLERY -> {
                if (resultCode == RESULT_OK) {
                    val imageUri = data!!.data
                    val filePath = intent.getStringExtra(Constants.EXTRA_DIRECTORY)

                    copyFromGalleryToSubjectFolder(imageUri, filePath)
                }
            }
        }
    }

    override fun onMenuButtonPressed(position: Int, itemId: Int) {
        when (itemId) {
            R.id.action_rename -> {
                val editText = EditText(this)
                editText.setText(files[position].file.nameWithoutExtension)
                display.showEditTextDialog(R.string.action_rename, R.string.text_hint, editText, {
                    if (editText.text.isNotEmpty()) {
                        files[position].renameFileTo(editText.text.toString())
                        myNotes.adapter.notifyDataSetChanged() // TODO: NOT UPDATING
                    } else
                        display.showSnackShort(R.string.error_empty)
                })
            }

            R.id.action_delete -> {
                display.showBasicDialog(R.string.action_confirm, R.string.note_confirm_delete_message, {
                    files[position].file.delete()
                    files.removeAt(position)
                    myNotes.adapter.notifyDataSetChanged()
                })
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (showFabs)
            disableFabs()
        else
            super.onBackPressed()
    }

    private fun addTextNote() {
        val editText = EditText(this)
        display.showEditTextDialog(R.string.note_add, R.string.note_hint, editText, {
            val noteName = editText.text.toString().replace(" ", "")
            if (noteName.isNotEmpty()) {
                addTextNote(noteName)
            } else
                display.showSnackShort(R.string.error_empty)
        })
    }


    private fun addTextNote(noteName: String) {
        var file = File("$directory${File.separator}$noteName.txt")
        var i = 1
        while (file.exists()) {
            file = File("$directory${File.separator}$noteName($i).txt")
            i++
        }

        val textIntent = Intent(this, TextActivity::class.java)
        textIntent.putExtra(Constants.EXTRA_DIRECTORY, file.absolutePath)
        textIntent.putExtra(Constants.EXTRA_CONTENT, FileInfo(file).toString())
        startActivityForResult(textIntent, Constants.REQUEST_CODE_NOTE)
    }

    private fun addPhotoNote() {
        val editText = EditText(this)
        display.showEditTextDialog(R.string.note_add, R.string.note_hint, editText, {
            val noteName = editText.text.toString().replace(" ", "")
            if (noteName.isNotEmpty())
                addPhotoNote(noteName)
            else
                display.showSnackShort(R.string.error_empty)
        })
    }

    private fun addPhotoNote(noteName: String) {
        var photoFile = File(directory)

        try {
            photoFile = File.createTempFile("${noteName}SA", ".jpg", photoFile)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.resolveActivity(packageManager)?.let {
            val photoUri = FileProvider.getUriForFile(this@NotesActivity,
                    "${applicationContext.packageName}.provider", photoFile)

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            takePictureIntent.putExtra(Constants.EXTRA_NOTE_NAME, noteName)
            intent.putExtra(Constants.EXTRA_DIRECTORY, photoFile.absolutePath)
            startActivityForResult(takePictureIntent, Constants.REQUEST_CODE_CAMERA)
        }
    }

    private fun renamePhotoFile(noteName: String, photoFile: File) {
        var file = File("$directory${File.separator}$noteName.jpg")
        var i = 1
        while (file.exists())
            file = File("$directory${File.separator}$noteName(${i++}).jpg")

        photoFile.renameTo(file)

        files.add(FileInfo(file.absolutePath))
        myNotes.adapter.notifyDataSetChanged()
    }

    private fun addGalleryNote() {
        val editText = EditText(this)
        display.showEditTextDialog(R.string.note_add, R.string.note_hint, editText, {
            val noteName = editText.text.toString().replace(" ", "")
            if (noteName.isNotEmpty())
                addGalleryNote(noteName)
            else
                display.showSnackShort(R.string.error_empty)
        })
    }

    private fun addGalleryNote(noteName: String) {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        var copyDir = "$directory${File.separator}$noteName.jpg"
        var tempFile = File(copyDir)

        var j = 1
        while (tempFile.exists()) {
            copyDir = "$directory${File.separator}$noteName(${j++}).jpg"
            tempFile = File(copyDir)
        }

        val chooserIntent = Intent.createChooser(getIntent, getString(R.string.choice_gallery))
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))
        intent.putExtra(Constants.EXTRA_DIRECTORY, copyDir)

        startActivityForResult(chooserIntent, Constants.REQUEST_CODE_GALLERY)

    }

    private fun copyFromGalleryToSubjectFolder(imageUri: Uri, filePath: String) {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        try {
            contentResolver.query(imageUri, filePathColumn, null, null, null).use { cursor ->
                cursor.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val picturePath = cursor.getString(columnIndex)

                val copyFrom = File(picturePath)
                val copyTo = File(filePath)

                val inStream = FileInputStream(copyFrom)
                val outStream = FileOutputStream(copyTo)

                val inChannel = inStream.channel
                val outChannel = outStream.channel

                inChannel.transferTo(0, inChannel.size(), outChannel)

                inStream.close()
                outStream.close()

                files.add(FileInfo(filePath))
                myNotes.adapter.notifyDataSetChanged()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
