package com.damsky.danny.schoolassist.ui.prefs

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.utils.Constants

class PreferencesActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private lateinit var appReference: App

    override fun onCreate(savedInstanceState: Bundle?) {
        appReference = application as App

        setTheme(appReference.preferencesHelper.getTheme())

        super.onCreate(savedInstanceState)

        fragmentManager.beginTransaction().replace(android.R.id.content, PreferencesFragment()).commit()
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        when (key) {
            Constants.PREFERENCE_APP_THEME -> {
                AppCompatDelegate.setDefaultNightMode(appReference.preferencesHelper.getNightMode())
                finish()
            }

            Constants.PREFERENCE_NOTIFICATION_DELTA ->
                appReference.setAllNotifications()
        }
    }

    class PreferencesFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences)
        }
    }
}
