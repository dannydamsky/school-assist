package com.damsky.danny.schoolassist.ui.splash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.ui.intro.IntroActivity
import com.damsky.danny.schoolassist.ui.main.MainActivity
import com.damsky.danny.schoolassist.utils.Constants

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        if ((application as App).preferencesHelper.isFirstRun())
            startActivityForResult(Intent(this, IntroActivity::class.java),
                    Constants.REQUEST_CODE_START_INTRO)
        else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK)
            startActivity(Intent(this, MainActivity::class.java))

        finish()
    }

    override fun onBackPressed() = Unit
}
