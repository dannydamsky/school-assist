package com.damsky.danny.schoolassist.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.view.*
import android.widget.AdapterView
import android.widget.ListView
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.dannydamskyutils.EditableDialog
import com.damsky.danny.dannydamskyutils.SpinnerDialog
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.data.db.enums.Day
import com.damsky.danny.schoolassist.ui.about.AboutActivity
import com.damsky.danny.schoolassist.ui.actions.ActionsActivity
import com.damsky.danny.schoolassist.ui.main.adapters.MainListViewAdapter
import com.damsky.danny.schoolassist.ui.notes.NotesActivity
import com.damsky.danny.schoolassist.ui.prefs.PreferencesActivity
import com.damsky.danny.schoolassist.ui.web.WebActivity
import com.damsky.danny.schoolassist.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var mSectionsPagerAdapter: SectionsPagerAdapter
    private lateinit var mToggle: ActionBarDrawerToggle

    private lateinit var appReference: App
    private lateinit var display: Display

    private lateinit var suggestions: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        appReference = application as App

        val pair: Pair<Int, Int> = appReference.preferencesHelper.getThemeAndDayNightModeNoActionBar()
        setTheme(pair.first)
        AppCompatDelegate.setDefaultNightMode(pair.second)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        suggestions = resources.getStringArray(R.array.subject_suggestions).toList() as ArrayList<String>

        display = Display(this, R.drawable.ic_about_launcher, mainContent)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        mToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close)
        drawerLayout.addDrawerListener(mToggle)
        mToggle.syncState()

        container.adapter = mSectionsPagerAdapter
        container.offscreenPageLimit = mSectionsPagerAdapter.count
        container.currentItem = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1

        navigationDrawer.setNavigationItemSelectedListener(this)

        if (appReference.appDbHelper.isEmpty())
            navigationDrawer.menu.findItem(R.id.action_quick).isEnabled = false

        fab.setOnClickListener { showAddLessonDialog() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_web_school ->
                startWebActivity(appReference.preferencesHelper.getSchoolUrl())

            R.id.action_web_smart ->
                startWebActivity(appReference.preferencesHelper.getSmartUrl())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings ->
                startActivity(Intent(this, PreferencesActivity::class.java))

            R.id.action_about ->
                startActivity(Intent(this, AboutActivity::class.java))

            R.id.action_quick -> {
                val intent = Intent(this, ActionsActivity::class.java)
                startActivityForResult(intent, Constants.REQUEST_CODE_START_ACTIONS)
            }
        }
        drawerLayout.closeDrawers()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.REQUEST_CODE_START_ACTIONS -> {
                resetViewPager()
                if (appReference.appDbHelper.isEmpty())
                    navigationDrawer.menu.findItem(R.id.action_quick).isEnabled = false
            }
        }
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationDrawer))
            drawerLayout.closeDrawers()
        else
            super.onBackPressed()
    }

    fun resetViewPager() {
        val temp = container.currentItem
        container.adapter = mSectionsPagerAdapter
        container.offscreenPageLimit = mSectionsPagerAdapter.count
        container.currentItem = temp
    }

    private fun showAddLessonDialog() {
        val schedule = appReference.appDbHelper.getLessons(container.currentItem)
        val lengthOfSchedule = schedule.size

        if (lengthOfSchedule >= Constants.MAX_LESSONS)
            display.showSnackLong(R.string.error_too_many)
        else {
            val availableLessonNumbers = ArrayList<String>(Constants.MAX_LESSONS - lengthOfSchedule)

            var startingPoint = 0
            for (lesson in schedule) {
                for (i in startingPoint until lesson.lessonNum)
                    availableLessonNumbers.add("$i")

                startingPoint = lesson.lessonNum + 1
            }

            for (i in startingPoint until Constants.MAX_LESSONS)
                availableLessonNumbers.add("$i")

            val lessonNames = appReference.appDbHelper.getLessonNames(suggestions)

            SpinnerDialog.Builder(this, availableLessonNumbers)
                    .setIcon(R.drawable.ic_about_launcher)
                    .setTitle(R.string.lesson_add)
                    .setMessage(R.string.lesson_num_choose)
                    .setPositiveButtonClick { _, position ->
                        EditableDialog.Builder(this, lessonNames)
                                .setIcon(R.drawable.ic_about_launcher)
                                .setTitle(R.string.lesson_add)
                                .setHint(R.string.lesson_enter)
                                .setPositiveButtonClick { userInput ->
                                    if (userInput.isNotEmpty()) {
                                        appReference.appDbHelper.insertLesson(userInput,
                                                availableLessonNumbers[position].toInt(),
                                                Day.values()[container.currentItem])
                                        val item = navigationDrawer.menu.findItem(R.id.action_quick)
                                        if (!item.isEnabled)
                                            item.isEnabled = true
                                        resetViewPager()
                                    } else
                                        display.showSnackLong(R.string.error_empty)
                                }.show()
                    }.show()
        }
    }

    private fun startWebActivity(url: String) {
        val school = Intent(this, WebActivity::class.java)
        school.putExtra(Constants.EXTRA_URL, url)
        startActivity(school)
    }

    class MainFragment : android.support.v4.app.ListFragment() {
        companion object {
            private const val ARG_SECTION_NUMBER = "section_number"

            fun newInstance(sectionNumber: Int): MainFragment {
                val fragment = MainFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }

        private lateinit var display: Display
        private lateinit var appReference: App
        private lateinit var activityReference: MainActivity

        override fun onAttach(context: Context?) {
            super.onAttach(context)
            activityReference = activity!! as MainActivity
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            registerForContextMenu(listView)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_main, container, false)
            initReferences()
            setAdapter()
            return rootView
        }

        override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
            super.onListItemClick(l, v, position, id)
            startNotesActivity(appReference.appDbHelper.getLessons(arguments!!.getInt(ARG_SECTION_NUMBER) - 1)
                    [position].lessonName)
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
            super.onCreateContextMenu(menu, v, menuInfo)
            val inflater = activity!!.menuInflater

            val selectedItem = appReference.appDbHelper
                    .getLessons(arguments!!.getInt(ARG_SECTION_NUMBER) - 1)[(menuInfo as AdapterView.AdapterContextMenuInfo).position]

            if (selectedItem.notify)
                inflater.inflate(R.menu.menu_main_longpress_notify, menu)
            else
                inflater.inflate(R.menu.menu_main_longpress, menu)
        }

        override fun onContextItemSelected(item: MenuItem): Boolean {
            val selectedItem = appReference.appDbHelper
                    .getLessons(activityReference.container.currentItem)[(item.menuInfo as AdapterView.AdapterContextMenuInfo).position]

            when (item.itemId) {
                R.id.setNotification -> {
                    display.showTimeDialog(R.string.lesson_notify_start, selectedItem.startHour, selectedItem.startMinute, { startHour, startMinute ->
                        display.showTimeDialog(R.string.lesson_notify_end, selectedItem.endHour, selectedItem.endMinute, { endHour, endMinute ->
                            appReference.appDbHelper.updateLesson(selectedItem, startHour, startMinute, endHour, endMinute)
                            activityReference.resetViewPager()
                            appReference.setNotification(selectedItem)
                        })
                    })
                }

                R.id.renameLesson -> {
                    val daoSession = appReference.appDbHelper.getDaoSession()
                    val lessonNames = appReference.appDbHelper.getLessonNames(activityReference.suggestions)

                    EditableDialog.Builder(context!!, lessonNames)
                            .setIcon(R.drawable.ic_about_launcher)
                            .setTitle(R.string.title_lesson_rename)
                            .setHint(R.string.lesson_enter)
                            .setText(selectedItem.lessonName)
                            .setPositiveButtonClick { userInput ->
                                if (userInput.isNotEmpty()) {
                                    appReference.appDbHelper.updateLesson(selectedItem, userInput, daoSession)
                                    activityReference.resetViewPager()
                                } else
                                    display.showSnackLong(R.string.error_empty)
                            }.show()
                }

                R.id.removeLesson ->
                    display.showBasicDialog(R.string.title_lesson_remove, R.string.lesson_remove_message, {
                        selectedItem.notify = false
                        appReference.setNotification(selectedItem)
                        appReference.appDbHelper.deleteLesson(selectedItem)
                        activityReference.resetViewPager()
                    })

                R.id.disableNotification -> {
                    selectedItem.notify = false
                    appReference.appDbHelper.updateLesson(selectedItem)
                    appReference.setNotification(selectedItem)
                    activityReference.resetViewPager()
                }
            }
            return true
        }

        private fun initReferences() {
            display = activityReference.display
            appReference = activityReference.appReference
        }

        private fun setAdapter() {
            val adapter = MainListViewAdapter(context!!, appReference.appDbHelper.getLessons(arguments!!.getInt(ARG_SECTION_NUMBER) - 1))
            listAdapter = adapter
        }

        private fun startNotesActivity(lessonName: String) {
            val file = File("${Environment.getExternalStorageDirectory()}${File.separator}SchoolAssist${File.separator}$lessonName")
            var fileExists = file.exists()

            if (!fileExists)
                fileExists = file.mkdirs()

            if (fileExists) {
                val notes = Intent(activity, NotesActivity::class.java)
                notes.putExtra(Constants.EXTRA_DIRECTORY, file.absolutePath)
                notes.putExtra(Constants.EXTRA_SUB_TITLE, lessonName)
                startActivity(notes)
            } else
                display.showSnackShort(R.string.error_dir)
        }
    }

    private inner class SectionsPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = MainFragment.newInstance(position + 1)

        override fun getCount(): Int = Constants.DAYS_IN_WEEK

        override fun getPageTitle(position: Int): CharSequence = resources.getStringArray(R.array.days)[position]
    }
}
