package com.damsky.danny.schoolassist.ui.times.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.times.listeners.TimesClickListener
import com.damsky.danny.schoolassist.utils.LessonNumber
import kotlinx.android.synthetic.main.activity_times_cardview.view.*

class TimesAdapter(private val onClickListener: TimesClickListener, private val contents: ArrayList<LessonNumber>) : RecyclerView.Adapter<TimesAdapter.CardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder =
            CardViewHolder(LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.activity_times_cardview, parent, false))

    override fun getItemCount() = contents.size

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) = holder.bind(contents, position, onClickListener)

    class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(contents: ArrayList<LessonNumber>, position: Int, onClickListener: TimesClickListener) = with(itemView) {
            val content = contents[position]
            lessonNumber.text = content.lessonNumber.toString()
            startButton.text = "${resources.getString(R.string.start_time)}: ${content.getStartTime()}"
            endButton.text = "${resources.getString(R.string.end_time)}: ${content.getEndTime()}"

            startButton.setOnClickListener {
                onClickListener.onStartTimeClicked(position)
            }

            endButton.setOnClickListener {
                onClickListener.onEndTimeClicked(position)
            }
        }
    }
}
