package com.damsky.danny.schoolassist.ui.notes.listeners

interface CardClickListener {
    fun onMenuButtonPressed(position: Int, itemId: Int)
}
