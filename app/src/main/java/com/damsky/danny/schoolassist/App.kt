package com.damsky.danny.schoolassist

import android.app.Application
import com.damsky.danny.schoolassist.data.db.AppDbHelper
import com.damsky.danny.schoolassist.data.db.model.DaoSession
import com.damsky.danny.schoolassist.data.db.model.Lesson
import com.damsky.danny.schoolassist.data.prefs.AppPreferencesHelper
import com.damsky.danny.schoolassist.utils.NotificationTimer
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

class App : Application() {
    lateinit var appDbHelper: AppDbHelper
    lateinit var preferencesHelper: AppPreferencesHelper

    override fun onCreate() {
        super.onCreate()

        appDbHelper = AppDbHelper(this)

        preferencesHelper = AppPreferencesHelper(this)

        setAllNotifications()
    }

    fun setAllNotifications() = launch {
        async {
            for (day in appDbHelper.getLessons())
                for (lesson in day)
                    setNotification(lesson)
        }.await()
    }

    fun setNotification(lesson: Lesson) = launch {
        async {
            NotificationTimer.setNotification(this@App, lesson, preferencesHelper.getNotificationDelta())
        }.await()
    }

    fun disableAllNotifications(daoSession: DaoSession = appDbHelper.getDaoSession()) {
        for (day in appDbHelper.getLessons())
            for (lesson in day)
                if (lesson.notify) {
                    lesson.notify = false
                    appDbHelper.updateLesson(lesson, daoSession)
                    NotificationTimer.setNotification(this, lesson, preferencesHelper.getNotificationDelta())
                }
    }

    fun enableAllNotifications(daoSession: DaoSession = appDbHelper.getDaoSession()) {
        for (day in appDbHelper.getLessons())
            for (lesson in day)
                if (!lesson.notify) {
                    lesson.notify = true
                    appDbHelper.updateLesson(lesson, daoSession)
                    NotificationTimer.setNotification(this, lesson, preferencesHelper.getNotificationDelta())
                }
    }

    fun deleteAllNotifications() {
        val daoSession = appDbHelper.getDaoSession()
        disableAllNotifications(daoSession)
        appDbHelper.deleteAllLessons(daoSession)
    }
}
