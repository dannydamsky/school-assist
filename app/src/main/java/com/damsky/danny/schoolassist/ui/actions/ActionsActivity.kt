package com.damsky.danny.schoolassist.ui.actions

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.actions.adapters.ActionsListViewAdapter
import com.damsky.danny.schoolassist.ui.times.TimesActivity
import com.damsky.danny.schoolassist.utils.Constants
import kotlinx.android.synthetic.main.activity_actions.*
import java.util.*

class ActionsActivity : AppCompatActivity(), AdapterView.OnItemClickListener {
    private lateinit var appReference: App
    private lateinit var display: Display

    override fun onCreate(savedInstanceState: Bundle?) {
        appReference = application as App

        setTheme(appReference.preferencesHelper.getThemeNoActionBar())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actions)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        display = Display(this, R.drawable.ic_about_launcher)

        val actions = Arrays.asList(*resources.getStringArray(R.array.quick_actions))
        val adapter = ActionsListViewAdapter(this, actions)
        actionsList.adapter = adapter
        actionsList.onItemClickListener = this
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (position) {
            Constants.ACTIONS_SET_NOTIFICATIONS -> {
                val intent = Intent(this, TimesActivity::class.java)
                startActivityForResult(intent, Constants.REQUEST_CODE_START_TIMES)
            }

            Constants.ACTIONS_DELETE_NOTIFICATIONS -> {
                display.showBasicDialog(R.string.action_confirm, R.string.lessons_delete_message, {
                    appReference.deleteAllNotifications()
                    display.showToastShort(R.string.lessons_deleted)
                    finish()
                })
            }

            Constants.ACTIONS_DISABLE_NOTIFICATIONS -> {
                display.showBasicDialog(R.string.action_confirm, R.string.lessons_disable_message, {
                    appReference.disableAllNotifications()
                    display.showToastShort(R.string.lessons_disabled)
                    finish()
                })
            }

            Constants.ACTIONS_ENABLE_NOTIFICATIONS -> {
                display.showBasicDialog(R.string.action_confirm, R.string.lessons_enable_message, {
                    appReference.enableAllNotifications()
                    display.showToastShort(R.string.lessons_enabled)
                    finish()
                })
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.REQUEST_CODE_START_TIMES -> {
                if (resultCode == RESULT_OK)
                    finish()
            }
        }
    }
}
