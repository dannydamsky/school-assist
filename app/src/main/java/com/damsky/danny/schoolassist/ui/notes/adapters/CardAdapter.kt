package com.damsky.danny.schoolassist.ui.notes.adapters

import android.app.Activity
import android.content.Intent
import android.support.v4.content.FileProvider
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.bumptech.glide.request.RequestOptions
import com.damsky.danny.schoolassist.GlideApp
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.ui.notes.listeners.CardClickListener
import com.damsky.danny.schoolassist.ui.text.TextActivity
import com.damsky.danny.schoolassist.utils.Constants
import com.damsky.danny.schoolassist.utils.FileInfo
import kotlinx.android.synthetic.main.activity_notes_cardview.view.*

class CardAdapter(private val onClickListener: CardClickListener, private val contents: ArrayList<FileInfo>) : RecyclerView.Adapter<CardAdapter.CardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder =
            CardViewHolder(LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.activity_notes_cardview, parent, false))

    override fun getItemCount(): Int = contents.size

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) =
            holder.bind(contents, position, onClickListener)

    class CardViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(contents: ArrayList<FileInfo>, position: Int, onClickListener: CardClickListener) = with(itemView) {
            val file = contents[position]

            fileName.text = file.file.nameWithoutExtension

            if (file.isText) {
                ifPhoto.visibility = View.GONE
                ifText.visibility = View.VISIBLE
                ifText.text = file.toString()
            } else {
                ifText.visibility = View.GONE
                ifPhoto.visibility = View.VISIBLE
                GlideApp.with(notes.context)
                        .load(file.file)
                        .apply(RequestOptions()
                                .fitCenter()
                                .override(512))
                        .into(ifPhoto)
            }

            notes.setOnClickListener {
                if (file.isText) {
                    val textActivity = Intent(notes.context, TextActivity::class.java)
                    textActivity.putExtra(Constants.EXTRA_DIRECTORY, file.file.absolutePath)
                    textActivity.putExtra(Constants.EXTRA_CONTENT, file.toString())
                    (notes.context as Activity).startActivityForResult(textActivity, Constants.REQUEST_CODE_NOTE)
                } else {
                    val intent = Intent()
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.action = Intent.ACTION_VIEW
                    intent.data = FileProvider.getUriForFile(notes.context,
                            "${notes.context.applicationContext.packageName}.provider", file.file)
                    notes.context.startActivity(intent)
                }
            }

            menuButton.setOnClickListener {
                val popupMenu = PopupMenu(context, menuButton)
                popupMenu.inflate(R.menu.menu_notes_popup)
                popupMenu.setOnMenuItemClickListener { item: MenuItem ->
                    onClickListener.onMenuButtonPressed(position, item.itemId)
                    true
                }

                popupMenu.show()
            }
        }

    }
}
