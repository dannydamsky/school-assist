package com.damsky.danny.schoolassist.ui.web

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.webkit.CookieManager
import android.webkit.URLUtil
import android.webkit.WebView
import android.webkit.WebViewClient
import com.damsky.danny.dannydamskyutils.Display
import com.damsky.danny.schoolassist.App
import com.damsky.danny.schoolassist.R
import com.damsky.danny.schoolassist.utils.Constants
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : AppCompatActivity() {
    private lateinit var defaultUrl: String
    private var currentUrl: String? = null
    private lateinit var display: Display

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme((application as App).preferencesHelper.getThemeNoActionBar())

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_web)

        setSupportActionBar(toolbar)

        display = Display(this, R.drawable.ic_about_launcher)

        activity_web.webViewClient = MyWebViewClient()

        defaultUrl = intent.getStringExtra(Constants.EXTRA_URL)
        currentUrl = intent.getStringExtra(Constants.EXTRA_URL_CURRENT)

        if (currentUrl == null)
            currentUrl = defaultUrl

        supportActionBar?.subtitle = cutProtocolFromUrl(currentUrl!!)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        activity_web.settings.loadWithOverviewMode = true
        activity_web.settings.loadsImagesAutomatically = true
        activity_web.settings.useWideViewPort = true
        activity_web.settings.builtInZoomControls = true
        activity_web.settings.displayZoomControls = false
        activity_web.settings.allowFileAccess = true
        activity_web.settings.javaScriptCanOpenWindowsAutomatically = false
        activity_web.settings.javaScriptEnabled = true

        activity_web.setDownloadListener { url, userAgent, contentDisposition, mimeType, _ ->
            if (storagePermissionGranted())
                download(userAgent, contentDisposition, mimeType, url)
            else
                display.showSnackLong(R.string.error_file_download)
        }

        activity_web.loadUrl(currentUrl)
    }

    override fun onPause() {
        intent.putExtra(Constants.EXTRA_URL_CURRENT, currentUrl)
        super.onPause()
    }

    override fun onBackPressed() {
        if (activity_web.canGoBack())
            activity_web.goBack()
        else
            finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_web, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        activity_web.loadUrl(defaultUrl)
        currentUrl = defaultUrl
        supportActionBar?.subtitle = cutProtocolFromUrl(defaultUrl)

        return super.onOptionsItemSelected(item)
    }

    private fun storagePermissionGranted(): Boolean =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            else
                true

    private fun cutProtocolFromUrl(url: String): String =
            url.substring(if (url.startsWith("https://")) 8 else 7)

    private fun download(UserAgent: String, ContentDisposition: String, MimeType: String, Url: String) {
        val stringToDisplay = resources.getString(R.string.file_download)

        val request = DownloadManager.Request(Uri.parse(Url))
        request.setMimeType(MimeType)
        request.addRequestHeader("cookie", CookieManager.getInstance().getCookie(Url))
        request.addRequestHeader("User-Agent", UserAgent)
        request.setDescription("$stringToDisplay...")
        request.setTitle(URLUtil.guessFileName(Url, ContentDisposition, MimeType))
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(Url, ContentDisposition, MimeType))

        val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        dm.enqueue(request)

        display.showSnackLong(stringToDisplay)
    }

    private inner class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            activity_web.loadUrl(url)
            currentUrl = url
            supportActionBar?.subtitle = cutProtocolFromUrl(url)

            return true
        }
    }
}
