package com.damsky.danny.schoolassist.ui.times.listeners

interface TimesClickListener {
    fun onStartTimeClicked(position: Int)
    fun onEndTimeClicked(position: Int)
}